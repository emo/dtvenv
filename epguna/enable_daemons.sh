#!/bin/bash

for sv in php-fpm nginx atd pcscd crond.service
do
  systemctl enable $sv
  systemctl start $sv
done

# 権限が付与されないことがあるため暫定対応
chmod 666 /dev/px4video*
