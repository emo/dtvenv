#!/bin/bash
# 字幕抽出定期実行用

# PATH追加(dockerfileでの追加は反映されないため)
export PATH=$PATH:/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin

# log保存先
LOGDIR=/var/log/trb/
LOGF=${LOGDIR}/$(date +%y%m).extass.log

{
  cd /mnt/tv/ && \
  ~/bin/filename_escape.rb ./ && \
  ~/bin/extass.rb ./ news && \
  ~/bin/mv_encorded_ts.rb ./ && \
  ~/bin/rmass.rb ./ && \
  echo "rm empty" && \
  find . -maxdepth 1 -size -1k | xargs -I{} rm -v {} && \
  echo complete
} 2>&1 | tee -a $LOGF
