#!/bin/ruby -Ku

require 'pathname'

# 拡張子を変更したPathnameを作成
class Pathname
  def chext(ext)
    self.parent + (self.basename(".*").to_s() + ext)
  end
end


# mp4があるtsを移動
def mv_encorded_ts(src, dest = nil)
  # destが無い場合はsrc + _encorded
  dest = if(dest) then
    Pathname(dest)
  else
    Pathname(src) + '_encorded'
  end

  Dir["#{src}/**/*.ts"].each{ |tsf|
    tsf = Pathname(tsf)
    m4f = tsf.chext(".mp4")
    # mp4が無いなら次
    next unless m4f.exist?
    # 移動処理
    puts("mv: #{tsf}")
    dest.mkpath
    tsf.rename(dest + tsf.basename)
  }
end


# 処理実行
mv_encorded_ts(*ARGV)

