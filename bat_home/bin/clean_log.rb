#!/bin/ruby -Ku

require 'mysql2'
require 'yaml'

# config読み込み
conf = YAML.load_file(File.dirname(__FILE__) + '/../anime_auto_rec/conf.yml')
# query結果取得
client = Mysql2::Client.new(conf[:client])
q = 'SELECT id,message FROM Recorder_logTbl ORDER BY id ASC'
res = client.query(q)

# res.each do |v| p v end ; exit # debug

# 加工
hash = {}
res.each do |h|
  hash[h['id']] = h['message']
end

# 再予約IDを検出
ids = res.find_all do |h|
  h['message'] =~ /再予約を試みます。/
end.map do |h|
  h['id']
end
# 消すID範囲を決定
id_beg = ids[0]
ranges = []
(1...ids.size).each do |i|
  next if ids[i] - ids[i-1] < 3
  ranges << ((id_beg + 2)...(ids[i-1]))
  id_beg = ids[i]
end
ranges << ((id_beg + 2)...(ids[-1])) if id_beg != ids[0]

# 結果出力
if false then # 必要でないなら無効
  ranges.each do |rg|
    ((rg.begin - 3)..(rg.end + 3)).each do |i|
      puts "#{rg === i ? 'D' : ' '}:#{i}:#{hash[i]}"
    end
  end
end

# do query
q = "DELETE FROM Recorder_logTbl WHERE id BETWEEN ? AND ?"
ranges.each do |rg|
  p client.prepare(q).execute(rg.first(), rg.last() -1)
end

