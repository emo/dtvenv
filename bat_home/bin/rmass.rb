#!/bin/ruby -Ku

require 'pathname'

# 拡張子を変更したPathnameを作成
class Pathname
  def chext(ext)
    self.parent + (self.basename(".*").to_s() + ext)
  end
end


# assだけのファイルを削除
def rmass(dir)
  Dir["#{dir}/**/*.ass"].each{ |assf|
    assf = Pathname(assf)
    # 対応する動画の検索. 無ければskip.
    mvf = %w!.mp4 .ts!.find{|ext|
      assf.chext(ext).exist?()
    }
    next if mvf
    # 削除
    puts("rm: #{assf}")
    assf.delete
  }
end

# 実行
rmass(*ARGV)

