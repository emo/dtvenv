#!/bin/ruby -Ku

# ext4では?がファイル名に利用できるがWindows系では利用できないしLinuxでも色々面倒なためrenameするやつ.
# 1階層のみ対応
# shellで書くと?を含むファイルをfindできないため割りと面倒.
# また録画では?が入るとなぜか.tsが.TSになるため拡張子も小文字化する
# usage: rename_question.rb <Directory 1> <Directory 2> ...

require 'pathname'

FILENAME_ESCAPE_TABLE = [
  '/:*?"<>|―',
  '_：＊？”＜＞｜-'
]

def filename_escape(base)
  # 一部文字はtrで処理できないためgsub
  base.tr(*FILENAME_ESCAPE_TABLE).gsub('\\', "￥").gsub("\n",'_')
end

def rename_file(path)
  name = path.basename.to_s
  name2 = filename_escape(name)
  if(name != name2)
    puts "#{name} => #{name2}"
    path.rename(path.parent + name2)
  end
end

def main
  if ARGV.empty?
    puts "${pathname($0).basename} <rename file/directories...>"
    return -1
  end

  ARGV.each{|f|
    f = Pathname(f)
    if (f.file?)
      rename_file(f)
    else
      Dir[f + "**/*"].each{|fn| rename_file(Pathname(fn)) }
    end
  }

end

def test
  s2 = filename_escape(FILENAME_ESCAPE_TABLE[0])
  puts FILENAME_ESCAPE_TABLE[0]
  puts s2
  puts FILENAME_ESCAPE_TABLE[1]
  s2 == FILENAME_ESCAPE_TABLE[1]
end
# p test

main if __FILE__ == $0


