#!/bin/ruby -Ku

require 'pathname'

# 拡張子を変更したPathnameを作成
class Pathname
  def chext(ext)
    self.parent + (self.basename(".*").to_s() + ext)
  end
end

def extass_file(tsf)
  # pathname化
  tsf = Pathname(tsf)
  # assがあるならskip
  assf = tsf.chext(".ass")
  return true if assf.exist?()
  # 処理
  puts("extract ass: #{tsf}")
  return system('arib2ass', '--file', tsf.to_s(), '--output', assf.to_s())
end

# 字幕抽出対象ファイル列挙し字幕抽出
def extass(dir)
  # 字幕抽出時に ./data ディレクトリが生成されるためchdirする
  Dir.chdir(dir) {
    # 1階層だけ[字]があるtsを探し処理
    Dir["./*[字]*.ts"].each{ |tsf|
      r = extass_file(tsf)
      unless r then exit(-1) end
    }
  }
end

# 処理実行
ARGV.each{ |d| extass(d) }
exit(0)
