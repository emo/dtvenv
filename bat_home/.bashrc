# .bashrc

# mysql用
export TERM=xterm

# 色
PS1="\e[35;1m\t \u@\h:\W \e[m\n$ "
alias ls="ls --color=auto"

# 起動直後にhomeに移動する(suで入った場合はrootにいる)
cd

