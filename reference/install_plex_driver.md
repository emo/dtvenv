# コンテナでの試行 → NG

```sh
# vmhostで実行
docker run --name centtest -ti --rm --privileged centos

# 以下をコンテナで実行
yum install -y gcc make unzip git wget

# install firmware
cd ~/
git clone https://github.com/nns779/px4_drv.git
cd px4_drv/fwtool/
make
wget http://plex-net.co.jp/download/pxw3pe4v1.4.zip
unzip pxw3pe4v1.4.zip
./fwtool x64/PXW3PE4.sys it930x-firmware.bin
mkdir -p /lib/firmware
cp it930x-firmware.bin /lib/firmware/

# install driver
yum install -y epel-release
yum install -y dkms udev kernel-devel-`uname -r` kernel-headers-`uname -r`
ln -s /usr/src/kernels/`uname -r` /lib/modules/
cp -a ~/px4_drv/ /usr/src/px4_drv-0.2.1
dkms add px4_drv/0.2.1
dkms install px4_drv/0.2.1
```

# vmhost内部で実行 → 無事に成功
補足
+ gitはvmhostに入れていなかったためcent-vm3で実行(NFS共有)
+ kernel-headersなどは依然の開発で導入済み


```sh
# install firmware
cd /data/src/archive/install
git clone https://github.com/nns779/px4_drv.git
cd px4_drv/fwtool/
make
wget http://plex-net.co.jp/download/pxw3pe4v1.4.zip
unzip -j pxw3pe4v1.4.zip x64/PXW3PE4.sys
./fwtool PXW3PE4.sys it930x-firmware.bin
sudo mkdir -p /lib/firmware
sudo cp it930x-firmware.bin /lib/firmware/
cd ..
# install driver
sudo cp -a ./ /usr/src/px4_drv-0.2.1
sudo yum install -y dkms
sudo dkms add px4_drv/0.2.1
  # => Creating symlink /var/lib/dkms/px4_drv/0.2.1/source -> /usr/src/px4_drv-0.2.1
DKMS: add completed.
sudo dkms install px4_drv/0.2.1
  # => DKMS: install completed.
sudo modprobe px4_drv
```

## しかし認識しないため診断

### [Raspberry Pi 3 Model B で PLEX PX-W3U4 が動くという良い話](https://hirooka.pro/raspberry-pi/raspberry-pi-plex-px-w3u4/)を元に確認
```sh
(host) $ lsmod | grep px
px4_drv               113291  0 

$ dmesg | grep px
[10738.941119] px4_drv: loading out-of-tree module taints kernel.
[10738.941627] px4_drv: module verification failed: signature and/or required key missing - tainting kernel
[10738.943667] px4_drv: px4_drv version 0.2.1
[10739.260933] px4_drv 1-7:1.0: Firmware loaded. version: 1.12.0.0
[10739.395400] px4_drv 1-7:1.0: tsdev 0: px4video0
[10739.395469] px4_drv 1-7:1.0: tsdev 1: px4video1
[10739.395510] px4_drv 1-7:1.0: tsdev 2: px4video2
[10739.395544] px4_drv 1-7:1.0: tsdev 3: px4video3
[10739.395605] usbcore: registered new interface driver px4_drv

(host) $ dmesg | grep usb
...
[    1.306549] usb 1-7: New USB device strings: Mfr=1, Product=2, SerialNumber=3
[    1.306551] usb 1-7: Product: PXW3PE4
```
→ 得に異常なし

```sh
(container) $ ls /dev/px* -l
crw------- 1 root root 242, 0 Jan 18 04:17 /dev/px4video0
```
→ 権限が無いだけだった

# 感度確認
* epgunaコンテナのnginxユーザーで実行
```sh
# BS
$ checksignal --device /dev/px4video0 101
C/N = 14.668133dB

$ checksignal --device /dev/px4video1 101
C/N = 14.668133dB

# 地上波 アッテネーター導入前
$ checksignal --device /dev/px4video2 22 
C/N = 14.098457dB

$ checksignal --device /dev/px4video3 22
C/N = 14.956392dB

# 地上波 10dBアッテネーター導入後
$ checksignal --device /dev/px4video3 22
C/N = 26.115793dB^C

```
