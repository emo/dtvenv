#!/bin/bash
# px4driverのpermissionを確認し600になっていれば666に戻す
# vscodeのコンテナで実行するためdocker execを主体とする

{
  if docker exec -u nginx epguna bash -c "test -w /dev/px4video0"; then
    echo "OK @ $(date +%F_%T)"
    exit 0
  else
    echo "NG @ $(date +%F_%T)"
    docker exec -u root epguna bash -c "ls -l /dev/px4* ; chmod 666 /dev/px4*"
    exit 1
  fi
} | tee -a /data/var/log/px4permission.log

