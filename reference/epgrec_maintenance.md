# epgrecメンテナンスコマンド

## チューナー関連
受信感度確認は https://investnora.blog.fc2.com/blog-entry-149.html を参考
```sh
# 地上波確認(checksignal)
docker exec -ti -u nginx epguna checksignal 22
# NHK(22ch)地上波を実際に10s録画
nginx $ recpt1 --b25 --strip 22 10 '/var/www/epgrec/video/tmp.ts' 

# BS感度確認
nginx $ checksignal --device /dev/px4video0 101
docker exec -ti -u nginx epguna checksignal 101
```

## epgrecシステム関連

### 番組表更新
http://(epgrec動作マシンIP:Host)/epgrec/shepherd.php にアクセス

または

```sh
nginx $ /var/www/epgrec/shepherd.php
```

### サーバー再起動
再起動前にコンテナをstopさせ再起動後にstartする
```sh
# PC電源ON-OFFでの再起動 ---------------------
docker-compose stop # 再起動前に実施
docker-compose start  # 再起動後の起動

# リビルドしてコンテナを組み直す場合 --------------------
docker-compose up -d --build
docker exec epguna /home/root/enable_daemons.sh
# atq登録
docker exec -u nginx epguna /var/www/epgrec/rev_test.php -r
# アニメ録画も再登録が必要
cd /data/src/dtv/epg-auto-reserve && docker-compose run --rm epg-auto-reserve ./epgar.py

# 再起動後 ---------------------
# px4の権限が変更されていることを確認. crw-rw-rwならOK.
docker exec -u root epguna bash -c "ls -la /dev/px4*"
# px4video～のpermissionが600の場合はchmodが必要
docker exec -u root epguna bash -c "chmod 666 /dev/px4*"
# atが登録されていることの確認
docker exec -u root epguna atq

```

# MariaDB

## terminal
```sh
docker exec -ti mariadb mysql -u root -p
nufuaaaa
```

## 構造確認
```SQL
show databases;
use epgrec
SHOW TABLES;
show columns from Recorder_logTbl;
```
```
+-------------------------+
| Tables_in_epgrec        |
+-------------------------+
| Recorder_categoryTbl    |
| Recorder_channelTbl     |
| Recorder_keywordTbl     |
| Recorder_logTbl         |
| Recorder_programTbl     |
| Recorder_reserveTbl     |
| Recorder_transcodeTbl   |
| Recorder_transexpandTbl |
+-------------------------+
```
### Recorder_logTbl
```
+---------+--------------+------+-----+---------------------+
| Field   | Type         | Null | Key | Default             |
+---------+--------------+------+-----+---------------------+
| id      | int(11)      | NO   | PRI | NULL                |
| logtime | datetime     | NO   |     | 1970-01-01 00:00:00 |
| level   | int(11)      | NO   | MUL | 0                   |
| message | varchar(512) | NO   |     |                     |
+---------+--------------+------+-----+---------------------+
```


## Export
```sh
docker exec -i mariadb bash -c "mysqldump -u root -p epgrec" > export.sql
nufuaaaa
```


## log
```SQL
-- log数確認
select count(*) from Recorder_logTbl;
-- log全削除
delete from Recorder_logTbl;
```

## 録画失敗Logの重複削除
```sh
docker exec -ti -u bat tvrecbat /home/bat/bin/clean_log.rb
```

## 登録チャネルの確認
Recorder_channelTblに記載あり。
mariaDBでは全角出力できないため他のコンテナを立てて接続を推奨
```sh
docker exec -ti mariadb mysql --user=root --password epgrec -e "select * from Recorder_channelTbl;"
```

```sh
docker exec -ti mariadb mysql -u root -p epgrec -e "show columns from Recorder_channelTbl;"
```
```
+--------------+--------------+------+-----+---------+----------------+
| Field        | Type         | Null | Key | Default | Extra          |
+--------------+--------------+------+-----+---------+----------------+
| id           | int(11)      | NO   | PRI | NULL    | auto_increment |
| type         | varchar(8)   | NO   | MUL | GR      |                |
| channel      | varchar(10)  | NO   | MUL | 0       |                |
| name         | varchar(512) | NO   |     | none    |                |
| channel_disc | varchar(128) | NO   | MUL | none    |                |
| sid          | varchar(64)  | NO   |     | hd      |                |
| skip         | tinyint(1)   | NO   | MUL | 0       |                |
+--------------+--------------+------+-----+---------+----------------+
```
